package filetransfer;

import filetransfer.inputstreamproviders.FTPInputStreamProvider;
import filetransfer.inputstreamproviders.FileInputStreamProvider;
import filetransfer.inputstreamproviders.HTTPInputStreamProvider;
import filetransfer.outputhandlers.FileOutputHandler;
import filetransfer.outputhandlers.StringOutputHandler;

/**
 Examples on how to use the libary.
 */
public class Examples {
    TransferManager transferManager = new TransferManager();

    /**
     * Demonstrates a download from HTTP to a file
     */
    public void transferFromHttpToFile() {

        String url = "http://myserver.com/mypath/myfil.zip";
        String fileName = "c:/test/myfilename.zip";

        FileOutputHandler fileOutputHandler = new FileOutputHandler(fileName) {
            public void transferFinished(Transfer t, String fileName) {
                System.out.println("FILE RECEIVED " + fileName);
            }

            public void transferFailed(Transfer t, Exception e) {
                System.out.println("File transfer failed");
                //do something about it..
            }

            public void progress(Transfer t, long bytes, long totalSize) {
                System.out.println("FTP TRANSFER: transfered " + bytes + " out of " + totalSize);
            }
        };
        Transfer httptransferToString = transferManager.transfer(new HTTPInputStreamProvider(url), fileOutputHandler);
        httptransferToString.startTransfer();
    }

    /**
     * Demonstrates download from FTP to file
     */
    public void transferFromFtpToFile() {
        //HTTP httptransfer to file
        String server = "myftpserver.com";
        String ftpFile = "myfile.zip";
        String toFile = ftpFile;
        String username = "myuserv";
        String password = "password";

        FileOutputHandler fileOutputHandler = new FileOutputHandler(toFile) {
            public void transferFinished(Transfer t, String fileName) {
                System.out.println("FTP FILE RECEIVED " + fileName);
            }

            public void transferFailed(Transfer t, Exception e) {
                System.out.println("FTP transfer failed");
                e.printStackTrace();
                //do something about it..
            }

            public void progress(Transfer t, long bytes, long totalSize) {
                System.out.println("FTP TRANSFER: transfered " + bytes + " out of " + totalSize);
            }
        };
        Transfer ftpTransferToFile = transferManager.transfer(new FTPInputStreamProvider(server, username, password, ftpFile), fileOutputHandler);
        ftpTransferToFile.startTransfer();
    }

    /**
     * Demonstrates download from HTTP to a stringbuffer
     */
    public void transferFronHttpToString() {
        String url = "http://myserver.com/mypath";

        //http transfer to string
        StringBuffer result = new StringBuffer();
        StringOutputHandler stringOutputHandler = new StringOutputHandler() {
            public void transferFinished(Transfer t, StringBuffer result) {
                System.out.println("Received the file as a string " + result);
            }

            public void transferFailed(Transfer t, Exception e) {
                System.out.println("File transfer failed");
                e.printStackTrace();
                //do something about it..
            }

            public void progress(Transfer t, long bytes, long totalSize) {
                System.out.println("Filetransfer: transfered " + bytes + " out of " + totalSize);
            }
        };

        Transfer stringtransfer = transferManager.transfer(new HTTPInputStreamProvider(url), stringOutputHandler);
        stringtransfer.startTransfer();
    }

    /**
     * Demonstrates copy of a file on the same machine or network drive.
     */
    public void copyFileExample() {
        //insert real filename here 
        String fileName = "c:/test/file1.zip";
        String copyTo = "c:/test/file2.zip";

        FileOutputHandler fileOutputHandler = new FileOutputHandler(copyTo) {
            public void transferFinished(Transfer t, String fileName) {
                System.out.println("File copied to " + fileName);
            }


            public void transferFailed(Transfer t, Exception e) {
                System.out.println("Copy failed");
                e.printStackTrace();
            }
        };
        Transfer transfer = transferManager.transfer(new FileInputStreamProvider(fileName), fileOutputHandler);
        transfer.startTransfer();
    }

    public static void main(String args[]) {
        Examples examples = new Examples();
        examples.transferFromHttpToFile();
        /*examples.transferFronHttpToString();
        examples.copyFileExample();
        examples.transferFromFtpToFile();*/
    }
}
