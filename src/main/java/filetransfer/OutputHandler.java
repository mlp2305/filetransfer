package filetransfer;

import java.io.IOException;
import java.io.OutputStream;

/**
A class to provide an OutputStream to a resource and to handle events concerning the transfer.
The generic argument should be the result that use expects after transfer. For example path to file, a string, an array containing data etc etc.
 */
public interface OutputHandler<V> {

    /**
     * Gets an output stream to a resource
     * @return the output stream to the resource
     * @throws IOException
     */
	public OutputStream getOutputStream() throws IOException;

    /**
     * Is being called after trasnfer. User should implement this method to be notified after the trasnfer.
     * @param transfer a handler to the transfer
     * @param result the result of the transfer, could be a path to a file or similar, depending of what kind of OutputHandler is being used.
     */
    public void transferFinished(Transfer transfer, V result);

    /**
     * Returns the result of this transfer (could be a path to a file or similar).
     * @return
     */
    public V getResult();

    /**
     * Is being called of transfer did not succeed. Should be implemented by the user.
     * @param transfer the transfer being used
     * @param e
     */
    public void transferFailed(Transfer transfer, Exception e);

    /**
     * Is being called whenever the transfer makes any progress
     * @param transfer the transfer being used
     * @param bytes  how many bytes were loaded ?
     * @param totalSize how many bytes in total
     */
    public void progress(Transfer transfer, long bytes, long totalSize);
}
