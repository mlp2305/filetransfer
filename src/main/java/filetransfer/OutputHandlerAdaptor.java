package filetransfer;

/**
 Adaptor class for OutputHandler
 */
public abstract class OutputHandlerAdaptor<V> implements OutputHandler<V> {


    public void transferFailed(Transfer t, Exception e) {
    }

    public void progress(Transfer t, long bytes, long totalSize) {
    }
}
