package filetransfer.outputhandlers;

import filetransfer.OutputHandler;

import java.io.IOException;
import java.io.OutputStream;

/**
 * A class that provides a OutputStream that writes to a StringBuffer. Useful
 * for loading text resources (like HTLM pages) to a string.
 */
public abstract class StringOutputHandler implements
		OutputHandler<StringBuffer> {
	/**
	 * Here the text is stored
	 */
	StringBuffer result = new StringBuffer();

	/**
	 * Gets the OutputStream that writes to a StringBuffer
	 * 
	 * @return the inputstream
	 * @throws IOException
	 */
	public OutputStream getOutputStream() throws IOException {
		return createOutputStream();

	}

	private OutputStream createOutputStream() {
		OutputStream outputStream = new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				result.append((char) b);
			}
		};
		return outputStream;
	}

	/**
	 * Gets the result of the transfer (A stringbuffer)
	 * 
	 * @return
	 */
	public StringBuffer getResult() {
		return result;
	}
}
