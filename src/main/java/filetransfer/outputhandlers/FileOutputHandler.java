package filetransfer.outputhandlers;

import filetransfer.OutputHandlerAdaptor;

import java.io.*;

/**
 Class the handles output to files.
 */
public abstract class FileOutputHandler extends OutputHandlerAdaptor<String> {
    /*
    The path to the file
     */
    String filePath;

    /**
     * Returns the path to the file
     * @return
     */
    public String getResult() {
        return filePath;
    }

    /**
     * Creates a new FileInputHandler
     * @param fileName the path to the file
     */
    public FileOutputHandler(String fileName) {
        this.filePath = fileName;
    }

    /**
     * Gets an OutputStream to the file.
     * @return the OutputStream to the file.
     * @throws IOException
     */
    public OutputStream getOutputStream() throws IOException {
        return new BufferedOutputStream(new FileOutputStream(filePath));
    }
}
