package filetransfer;

import filetransfer.inputstreamproviders.InputStreamProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class for organizing a number of Transfers.
 */
public class TransferManager {
    private int nextId = 0;
    private Map<Integer, Transfer> transfersMap = new HashMap<Integer, Transfer>();

    /**
     * Creates a new instance of this class
     * @param inputStreamProvider 
     * @param outputHandler
     * @return
     */
    public Transfer transfer(InputStreamProvider inputStreamProvider, OutputHandler outputHandler) {
        int id = getNextId();
        Transfer transfer = new Transfer(inputStreamProvider, outputHandler, id);
        add(transfer);
        return transfer;
    }

    /**
     * Returns a list containing all Transfers
     * @return a list of transfers
     */
    public List<Transfer> getAll() {
        //make a copy of the maps value, so it's not possible to tamper with the maps collection of values.
        return new ArrayList<Transfer>(transfersMap.values());
    }

    /**
     * Gets all active transfers.
     * @return a list of transfers
     */
    public List<Transfer> getActive() {
        List<Transfer> active = new ArrayList<Transfer>();
        List<Transfer> transfers = getAll();
        for (Transfer transfer : transfers) {
            if (!transfer.isStopped()) {
                active.add(transfer);
            }

        }
        return active;
    }

    /**
     * The number of transfers, inactive or active
     * @return the number of transfers
     */
    public int getSize() {
        return transfersMap.size();
    }

    /**
     * Get the number of active transfers
     * @return the number of active
     */
    public int getNumberOfActive() {
        return getActive().size();
    }

    /**
     * Gets the number of inactive transfers
     * @return number of inactive transfers
     */
    public int getNumberOfInactive() {
        return getSize()-getNumberOfActive();
    }

    /**
     * adds a new transfers to the TransferManager(only being used internally)
     * @param transfer
     */
    private void add(Transfer transfer) {
        transfersMap.put(transfer.getId(), transfer);
    }

    /**
     * Gets next available transfer id
     * @return next available transfer id
     */
    synchronized private int getNextId() {
        //it's synchronized so that two transfers cannot get the same id.
        return nextId++;
    }

    public void removeAllInactive() {
        List<Transfer> transferList = getAll();
        for (Transfer transfer : transferList) {
            if (transfer.isStopped()) {
                removeFromTransferManager(transfer.getId());
            }

        }
    }

    public void removeAndStop(int id) {
        Transfer transfer = get(id);
        if (transfer != null) {
            transfer.stopTransfer();
            removeFromTransferManager(id);
        }
    }

    public void stop(int id) {
        Transfer transfer = get(id);
        if (transfer != null) {
            transfer.stopTransfer();
        }
    }

    public void stopAll() {
        List<Transfer> transferList = getActive();
        for (Transfer transfer : transferList) {
            if (!transfer.isStopped()) {
                transfer.stopTransfer();
            }
        }
    }

    public Transfer get(int id) {
        return transfersMap.get(id);
    }

    private void removeFromTransferManager(int id) {
        transfersMap.remove(id);
    }
}

