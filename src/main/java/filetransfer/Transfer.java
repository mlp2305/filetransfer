package filetransfer;

import filetransfer.inputstreamproviders.InputStreamProvider;

import java.io.*;

/**
 Class that transfers (download/copy etc) resources using an InputStreamProvider and an OutputHander.
 */
public class Transfer implements Runnable {
    int id;
    boolean stopped = false;
    OutputHandler outputHandler;
    InputStreamProvider inputStreamProvider;

    /**
     * Gets the ID of transfer
      * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return true of the transfer has stopped
     */
    public boolean isStopped() {
        return stopped;
    } 

    public void run() {
        try {
            performTransfer();
        } catch (Exception e) {

            failed(e);
        }
        cleanup();
    }

    /**
     * Creates a new transfer
     * @param inputStreamProvider
     * @param outputStreamProvider
     * @param id
     */
    public Transfer(InputStreamProvider inputStreamProvider, OutputHandler outputStreamProvider, int id) {
        this.id = id;
        this.outputHandler = outputStreamProvider;
        this.inputStreamProvider = inputStreamProvider;
    }

    /**
     * Performs cleanup
     */
    protected void cleanup() {
        inputStreamProvider.transferFinished();
    }

    /**
     * Starts transfer in a new thread.
     */
    public void startTransfer() {
        Thread t = new Thread(this);
        t.start();
    }

    /**
     * Stops the transfer
     */
    public void stopTransfer() {
        stopped = true;
    }


    protected void failed(Exception e) {
        outputHandler.transferFailed(this, e);
        stopped = true;
    }

    /**
     * Perform the actual transfer, not that calling this method will start the transfer in the current thread.
     * To start the transfer synchrone call startTransfer instead
     * @throws Exception
     */
    public void performTransfer() throws Exception {

        //transfer
        //get inputstream
        BufferedInputStream inputStream = new BufferedInputStream(inputStreamProvider.getInputStream());
        //get outputstream
        OutputStream outputStream = new BufferedOutputStream(outputHandler.getOutputStream());
        //get size
        long size = inputStreamProvider.getLength();
        long totalTransfered = 0;
        int bytesRead = 0;
        int chunks = 2048;
        byte[] tmp = new byte[chunks];
        //read bytes from inputstream
        while ((bytesRead = inputStream.read(tmp)) != -1 & !stopped) {
            totalTransfered += bytesRead;
            //write bytes to outputstream
            outputStream.write(tmp, 0, bytesRead);
            //notify about progress
            outputHandler.progress(this, totalTransfered, size);
        }
        //transfer done, flush and close
        outputStream.flush();
        outputStream.close();
        //only notify outputhandler about transfer finished if transfer wasn't stopped (then it's not complete)
        if (!stopped) {
            //get result, and notify outputhandler (user is waiting for this event).
            outputHandler.transferFinished(this, outputHandler.getResult());
        }

    }
}
