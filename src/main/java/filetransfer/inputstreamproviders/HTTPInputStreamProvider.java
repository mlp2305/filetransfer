package filetransfer.inputstreamproviders;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A class for downloading resources via HTTP.
 * 
 */
public class HTTPInputStreamProvider implements InputStreamProvider {
	private String url;
	private HttpEntity entity = null;

	/**
	 * Creates a mew HTTPInputStreamProvider
	 * 
	 * @param httpUrl
	 *            the URL to the HTTP resource
	 */
	public HTTPInputStreamProvider(String httpUrl) {
		this.url = httpUrl;
	}

	/**
	 * Gets a InputStream to the HTTP resource
	 * 
	 * @return
	 * @throws IOException
	 */
	public InputStream getInputStream() throws IOException {
		return new BufferedInputStream(getHTTPEntity().getContent());
	}

	/**
	 * Creates a new HTTP entity (only being used by this class or subclasses)
	 * 
	 * @return
	 * @throws IOException
	 */
	protected HttpEntity getHTTPEntity() throws IOException {
		HttpClient httpclient = createHttpClient();
		HttpGet httpget = new HttpGet(url);
		HttpResponse response = httpclient.execute(httpget);
		return response.getEntity();
	}

	public HttpClient createHttpClient() {
		return new DefaultHttpClient();
	}

	/**
	 * This Is being called after end transfer.
	 */
	public void transferFinished() {
	}

	/**
	 * Gets the size of the HTTP request in bytes
	 * 
	 * @return the size of the HTTP request in bytes
	 * @throws Exception
	 */
	public long getLength() throws Exception {
		return getHTTPEntity().getContentLength();
	}
}
