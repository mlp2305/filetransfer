package filetransfer.inputstreamproviders;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;
import java.io.InputStream;

/**
 * Provides a stream to a file on a FTP server
 */
public class FTPInputStreamProvider implements InputStreamProvider {
	String server;
	String username;
	String password;
	String file;
	FTPClient ftpClient;

	/**
	 * Creates a new FTPInputStreamProvider
	 * 
	 * @param server
	 *            the FTP server
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @param file
	 *            the file to download
	 */
	public FTPInputStreamProvider(String server, String username,
			String password, String file) {
		this.server = server;
		this.username = username;
		this.password = password;
		this.file = file;
	}

	/**
	 * Is being called after transfer, logs out and disconnects from the FTP
	 * server
	 */

	public void transferFinished() {

	}

	public void closeFtpClient() {
		try {
			getFtpClient().logout();
			getFtpClient().disconnect();
			ftpClient = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets an InputStream to the FTPServer
	 * 
	 * @return the InputStream to the FTPServer
	 * @throws IOException
	 */
	public InputStream getInputStream() throws IOException {
		return getFtpClient().retrieveFileStream(file);
	}

	public FTPClient getFtpClient() throws IOException {
		if (ftpClient == null) {
			ftpClient = createFtpClient();
			connect();
		}
		return ftpClient;
	}

	public void connect() {
		try {
			ftpClient.connect(server);
			ftpClient.login(username, password);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public FTPClient createFtpClient() {
		FTPClient client = new FTPClient();
		return client;
	}

	public long getLength() throws Exception {
		// TODO get the size of the file
		return -1;
	}

	public static void main(String args[]) throws Exception {
		FTPInputStreamProvider ftpInputStreamProvider = new FTPInputStreamProvider(
				"3dboxing.com", "mikkelp", "password",
				"scala-library-2.9.0.jar");
		ftpInputStreamProvider.getInputStream().read();
	}
}
