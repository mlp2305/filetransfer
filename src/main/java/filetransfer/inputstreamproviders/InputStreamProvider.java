package filetransfer.inputstreamproviders;

import java.io.IOException;
import java.io.InputStream;

/**
 Base class for retrieving the InputStream a Transfer needs.
 */
public interface InputStreamProvider {
    /**
     * Gets the InputStream
     * @return  the inputstream
     * @throws IOException
     */
    InputStream getInputStream() throws IOException;

    /**
     * This is being called when a transfer is finished. Cleanup code should be put here.
     */
     void transferFinished();

    /**
     * Gets the length of a transfer in bytes
     * @return  the length of a transfer in bytes. Should return -1 if no length is available.
     * @throws Exception
     */
    long getLength() throws Exception;
}
