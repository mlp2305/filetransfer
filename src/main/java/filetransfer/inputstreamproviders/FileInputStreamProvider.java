package filetransfer.inputstreamproviders;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 Provides a FileInputStream.
 */
public class FileInputStreamProvider implements InputStreamProvider {
    String fileName;
    /**
     * Creates a new instance of this class
     * @param fileName the name of the file.
     */
    public FileInputStreamProvider(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Is being called when transfer is finished.
     */
    public void transferFinished() {

    }

    /**
     * Returns an InputStream (A FileInputStream in this case)
     * @return the inputstream.
     * @throws IOException
     */
    public InputStream getInputStream() throws IOException {
        return new FileInputStream(new File(fileName));
    }

    /**
     * Gets the length of the file
     * @return the length of the of file
     * @throws Exception
     */
    public long getLength() throws Exception {
        return new File(fileName).length();
    }
}
