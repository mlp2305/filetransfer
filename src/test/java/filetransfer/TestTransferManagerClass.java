package filetransfer;

import junit.framework.Assert;
import filetransfer.inputstreamproviders.InputStreamProvider;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 Tests the Transfermanager.
 */
public class TestTransferManagerClass {
    //we need to put error codes here since all out feedback from the transfermanager are done via innner classes.
    boolean valuesTransferredCorrectly = false;
    boolean transferFinished = false;
    boolean transferFailed = false;

    /**
     * Tests the functionality of TransferManager by starting a transfer.
     * @throws Exception
     */
    @Test
    public void testTransfer() throws Exception {


        boolean transferFinishedInputStreamProviderM;
        boolean transferFinishedOutputHandler;

        final int[] values = {1, 2, 3, 4, 5, 6, 8, 9, 10};
        final ListOutputStream arrayOutputStream = new ListOutputStream();
        TransferManager transferManager = new TransferManager();
        Transfer transfer = transferManager.transfer(new InputStreamProvider() {
                    public InputStream getInputStream() throws IOException {

                        return new ArrayInputStream(values);

                    }

                    public void transferFinished() {
                        //no cleanup code necessary
                    }

                    public long getLength() throws Exception {
                        return values.length;
                    }
                }, new OutputHandler<List<Integer>>() {

            public OutputStream getOutputStream() throws IOException {
                return arrayOutputStream;
            }

            public void transferFinished(Transfer transfer, List<Integer> result) {
                transferFinished = true;
                //perform a sinple size check, List result should be the same size as the "values" arrays.
                if (result.size()== values.length) {
                    valuesTransferredCorrectly = true;
                    //TODO check the array "values" with the "result" list..they should contain same values..
                }
            }

            public List<Integer> getResult() {

                return arrayOutputStream.getValues();
            }

            public void transferFailed(Transfer transfer, Exception e) {
                transferFailed = true;
            }

            public void progress(Transfer transfer, long bytes, long totalSize) {

            }
        }
        );
        // we can't start the test asynchronous in a test..
        //starts the transfer, our listeners will pick up the events and alter our error/success variables.
        transfer.performTransfer();
        //check how it went..
        Assert.assertTrue(transferFinished);
        Assert.assertTrue(valuesTransferredCorrectly);
        Assert.assertFalse(transferFailed);
      }
}
