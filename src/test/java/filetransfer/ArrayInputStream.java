package filetransfer;

import java.io.IOException;
import java.io.InputStream;

/**
 * A simple array input stream. Reads its values from a int array.
 */
public class ArrayInputStream extends InputStream{
    int [] values;
    int counter;

    /**
     * Creates a new ArrayInputStream
      * @param values
     */
    public ArrayInputStream(int[] values) {
        this.values = values;
    }

    @Override
    public int read() throws IOException {
        if(counter<values.length) {
            return values[counter++];
        }else {
            return -1;
        }
        
    }
}
