package filetransfer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 An outputstream that simply writes to a list of integer.
 */
public class ListOutputStream extends OutputStream {
    List<Integer> values = new ArrayList<Integer>();

    public List<Integer> getValues() {
        return values;
    }

    @Override
    public void write(int b) throws IOException {
        values.add(b);
    }

}
